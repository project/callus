<?php

namespace Drupal\callus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure call us settings for this site.
 */
class CallUsForm extends ConfigFormBase {

  /**
   * Implements getEditableConfigNames().
   */
  protected function getEditableConfigNames() {
    return [
      'callus.settings',
    ];
  }

  /**
   * Implements getFormId().
   */
  public function getFormId() {
    return 'callus_form';
  }

  /**
   * Implements buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('callus.settings');
    // Form for the button setting.
    $form['callus_custom_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Callus Button Settings'),
      '#open' => TRUE,
    ];
    $form['callus_custom_settings']['cmsPhoneNumber'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number'),
      '#description' => $this->t('Call Us Phone Number Add.'),
      '#default_value' => $config->get('cmsPhoneNumber'),
      '#required' => TRUE,
    ];

    $form['callus_custom_settings']['cmsButtonLable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Lable'),
      '#description' => $this->t('Call Us Button Lable Custome Name'),
      '#default_value' => $config->get('cmsButtonLable'),
    ];
    $form['callus_custom_settings']['cmsButtonSide'] = [
      '#title' => $this->t('Button Side'),
      '#type' => 'select',
      '#description' => $this->t('Select the Button Side.'),
      '#options' => [
        1 => $this->t('Right'),
        2 => $this->t('Flot Right'),
        3 => $this->t('Left'),
      ],
      '#default_value' => $config->get('cmsButtonSide'),
    ];
    $form['callus_custom_settings']['cmsColorPicker'] = [
      '#type' => 'color',
      '#title' => $this->t('Choose Button Background Color'),
      '#description' => $this->t('Choose Button Background Color.'),
      '#default_value' => $config->get('cmsColorPicker'),
    ];
    $form['callus_custom_settings']['cmsColorFont'] = [
      '#type' => 'color',
      '#title' => $this->t('Color For Font'),
      '#description' => $this->t('Choose Font Color.'),
      '#default_value' => $config->get('cmsColorFont'),
    ];
    // Social Button Settings form.
    $form['social_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Social Media settings (Optional)'),
    ];
    $form['social_settings']['cmsFacebook'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook'),
      '#description' => $this->t('Social Button For Facebook Link. Ex: https://www.facebook.com/'),
      '#default_value' => $config->get('cmsFacebook'),
    ];
    $form['social_settings']['cmsGmail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gmail'),
      '#description' => $this->t('Social Button For Gmail Link. Ex: https://www.google.com/'),
      '#default_value' => $config->get('cmsGmail'),
    ];
    $form['social_settings']['cmsTwitter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter'),
      '#description' => $this->t('Social Button For Twitter Link. Ex: https://www.twitter.com/'),
      '#default_value' => $config->get('cmsTwitter'),
    ];
    $form['social_settings']['cmsLinkedIn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Linkedin'),
      '#description' => $this->t('Social Button For Linkedin Link. Ex: https://www.linkedin.com/'),
      '#default_value' => $config->get('cmsLinkedIn'),
    ];
    $form['social_settings']['cmsYoutube'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Youtube'),
      '#description' => $this->t('Social Button For Youtube Link. Ex: https://www.youtube.com/'),
      '#default_value' => $config->get('cmsYoutube'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implement validateForm().
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $phone = $form_state->getValue('cmsPhoneNumber');
    if (strlen($phone) < 10) {
      $form_state->setErrorByName('cmsPhoneNumber', $this->t('Mobile number is too short.'));
    }
    if (!preg_match("/^[0-9]*$/", $phone)) {
      $form_state->setErrorByName('cmsPhoneNumber', $this->t('Mobile number is only numeric Valid.'));
    }
  }

  /**
   * Implement submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Getting Value for button.
    $form_values = $form_state->getValues();

    $this->config('callus.settings')
      // Set button value setting.
      ->set('cmsPhoneNumber', $form_values['cmsPhoneNumber'])
      ->set('cmsButtonLable', $form_values['cmsButtonLable'])
      ->set('cmsButtonSide', $form_values['cmsButtonSide'])
      ->set('cmsColorPicker', $form_values['cmsColorPicker'])
      ->set('cmsColorFont', $form_values['cmsColorFont'])
      // Social set varible value.
      ->set('cmsFacebook', $form_values['cmsFacebook'])
      ->set('cmsGmail', $form_values['cmsGmail'])
      ->set('cmsTwitter', $form_values['cmsTwitter'])
      ->set('cmsLinkedIn', $form_values['cmsLinkedIn'])
      ->set('cmsYoutube', $form_values['cmsYoutube'])
      ->save();
    parent::submitForm($form, $form_state);

  }

}
