(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.callus = {

    attach: function (context, settings) {

      $(document).ready(function () {

        if ($('body').length && !($('#cms_callus').length)) {
          var cmsPhoneNumber = drupalSettings.cmsPhoneNumber;
          var cmsButtonLable = drupalSettings.cmsButtonLable;

          $('body').append('<a href="tel:' + cmsPhoneNumber + '" class="callus" id="cms_callus"><i class="fa fa-phone hvr-icon">  ' + cmsButtonLable + '</i></a><div class="icon-bar" id="cmssocial"></div>');

          var cmsButtonSide = drupalSettings.cmsButtonSide;
          var cmsColorPicker = drupalSettings.cmsColorPicker;
          var cmsColorFont = drupalSettings.cmsColorFont;

          // Social Icon.
          var cmsFacebook = drupalSettings.cmsFacebook;
          var cmsGmail = drupalSettings.cmsGmail;
          var cmsTwitter = drupalSettings.cmsTwitter;
          var cmsLinkedIn = drupalSettings.cmsLinkedIn;
          var cmsYoutube = drupalSettings.cmsYoutube;
          const CMS_MQ = window.matchMedia('(min-width: 580px)');

          if (cmsColorFont) {
            $('.callus').css({
              color: cmsColorFont
            });
          }
          if (cmsButtonSide === '1') {
            $('.callus').css({
              'right': '100px',
              'background-color': cmsColorPicker
            });
          }
          else if (cmsButtonSide === '2') {
            if (CMS_MQ.matches) { // If media query matches.
              $('.callus').css({
                'right': '-60px',
                'height': '40px',
                'transform': 'rotate(-90deg)',
                'bottom': '50%',
                'line-height': '40px',
                'background-color': cmsColorPicker
              });
            }
            else {
              $('.callus').css({
                'right': '100px',
                'background-color': cmsColorPicker
              });
            }
          }
          else {
            $('.callus').css({
              'left': '100px',
              'background-color': cmsColorPicker
            });
          }

          if (cmsFacebook) {
            $('#cmssocial').append('<a href="' + cmsFacebook + '" class="facebook"><i class="fa fa-facebook"></i></a>');

          }
          if (cmsGmail) {
            $('#cmssocial').append('<a href="' + cmsGmail + '" class="google"><i class="fa fa-google"></i></a>');

          }
          if (cmsTwitter) {
            $('#cmssocial').append('<a href="' + cmsTwitter + '" class="twitter"><i class="fa fa-twitter"></i></a>');

          }
          if (cmsLinkedIn) {
            $('#cmssocial').append('<a href="' + cmsLinkedIn + '" class="linkedin"><i class="fa fa-linkedin"></i></a>');

          }
          if (cmsYoutube) {
            $('#cmssocial').append('<a href="' + cmsYoutube + '" class="youtube"><i class="fa fa-youtube"></i></a>');
          }

        }

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
